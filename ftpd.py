# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# Copyright (c) Twisted Matrix Laboratories.
# See LICENSE for details.

"""
An example FTP server with minimal user authentication.
"""

import os
import time
import re
import stat
import errno
import fnmatch

from accounts import models as umodels
from commpute import models as cmodels

try:
    import pwd
    import grp
except ImportError:
    pwd = grp = None

from zope.interface import Interface, implements

# Twisted Imports
from twisted import copyright
from twisted.internet import reactor, interfaces, protocol, error, defer
from twisted.protocols import basic, policies

from twisted.python import log, failure, filepath

from twisted.cred import error as cred_error, portal, credentials, checkers

# , IRealm
from twisted.protocols.ftp import FTPFactory, FTPRealm, IFTPShell, IReadFile
from twisted.cred.portal import Portal
from twisted.cred.checkers import AllowAnonymousAccess, FilePasswordDB
from twisted.internet import reactor


class IWriteFile(Interface):

    """
    A file into which bytes may be written.
    """

    def receive():
        """
        Create a consumer which will write to this file.  This method may
        only be invoked once on each provider.

        @rtype: C{Deferred} of C{IConsumer}
        """

    def close():
        """
        Perform any post-write work that needs to be done. This method may
        only be invoked once on each provider, and will always be invoked
        after receive().

        @rtype: C{Deferred} of anything: the value is ignored. The FTP client
        will not see their upload request complete until this Deferred has
        been fired.
        """


class SingleFileRequestWrite():

    def openfile():

    def done(self):
      ##
      # Ok we shall publish the request..
      ##


class FileConsumer(object):

    """
    A consumer for FTP input that writes data to a file.

    @ivar fObj: a file object opened for writing, used to write data received.
    @type fObj: C{file}
    """

    implements(interfaces.IConsumer)

    def __init__(self, fObj):
        self.fObj = fObj

    def registerProducer(self, producer, streaming):
        self.producer = producer
        assert streaming

    def unregisterProducer(self):
        self.producer = None
        self.fObj.close()

    def write(self, bytes):
        self.fObj.write(bytes)


class _FileWriter(object):
    implements(IWriteFile)

    def __init__(self, fObj):
        self.fObj = fObj
        self._receive = False

    def receive(self):
        assert not self._receive, "Can only call IWriteFile.receive *once* per instance"
        self._receive = True
        # FileConsumer will close the file object
        return defer.succeed(FileConsumer(self.fObj))

    def close(self):
        return defer.succeed(None)


class _FileReader(object):
    implements(IReadFile)

    def __init__(self, fObj):
        self.fObj = fObj
        self._send = False

    def _close(self, passthrough):
        self._send = True
        self.fObj.close()
        return passthrough

    def send(self, consumer):
        assert not self._send, "Can only call IReadFile.send *once* per instance"
        self._send = True
        d = basic.FileSender().beginFileTransfer(self.fObj, consumer)
        d.addBoth(self._close)
        return d


class WIOFTPUserDatabaseDontUse:

    """
    An extremely simple credentials checker.

    This is only of use in one-off test programs or examples which don't
    want to focus too much on how credentials are verified.

    You really don't want to use this for anything else.  It is, at best, a
    toy.  If you need a simple credentials checker for a real application,
    see L{FilePasswordDB}.
    """

    implements(checkers.ICredentialsChecker)

    credentialInterfaces = (credentials.IUsernamePassword,
                            credentials.IUsernameHashedPassword)

    def __init__(self, dbconnection):
        self.dbconnection = dbconnection

    # def addUser(self, username, password):
        #self.users[username] = password

    def _cbPasswordMatch(self, matched, username):
        if matched:
            return username
        else:
            return failure.Failure(error.UnauthorizedLogin())

    def requestAvatarId(self, credentials):
        if credentials.username in self.users:
            return defer.maybeDeferred(
                credentials.checkPassword,
                self.users[credentials.username]).addCallback(
                self._cbPasswordMatch, str(credentials.username))
        else:
            return defer.fail(error.UnauthorizedLogin())


# TWISTED PROTOCOL ROOT FOR SFTP


class WIOFTPAnonymousShell(object):

    """
    An anonymous implementation of IFTPShell

    @type filesystemRoot: L{twisted.python.filepath.FilePath}
    @ivar filesystemRoot: The path which is considered the root of
    this shell.
    """
    implements(IFTPShell)

    def __init__(self, filesystemRoot):
        self.filesystemRoot = filesystemRoot
        self.fsmode = False

    def _path(self, path):
        return self.filesystemRoot.descendant(path)

    def makeDirectory(self, path):
        return defer.fail(AnonUserDeniedError())

    def removeDirectory(self, path):
        return defer.fail(AnonUserDeniedError())

    def removeFile(self, path):
        return defer.fail(AnonUserDeniedError())

    def rename(self, fromPath, toPath):
        return defer.fail(AnonUserDeniedError())

    def receive(self, path):
        path = self._path(path)
        return defer.fail(AnonUserDeniedError())

    def openForReading(self, path):
        """
        Open C{path} for reading.

        @param path: The path, as a list of segments, to open.
        @type path: C{list} of C{unicode}
        @return: A L{Deferred} is returned that will fire with an object
            implementing L{IReadFile} if the file is successfully opened.  If
            C{path} is a directory, or if an exception is raised while trying
            to open the file, the L{Deferred} will fire with an error.
        """
        p = self._path(path)
        if p.isdir():
            # Normally, we would only check for EISDIR in open, but win32
            # returns EACCES in this case, so we check before
            return defer.fail(IsADirectoryError(path))
        try:
            f = p.open('r')
        except (IOError, OSError), e:
            return errnoToFailure(e.errno, path)
        except:
            return defer.fail()
        else:
            return defer.succeed(_FileReader(f))

    def openForWriting(self, path):
        """
        Reject write attempts by anonymous users with
        L{PermissionDeniedError}.
        """
        return defer.fail(PermissionDeniedError("STOR not allowed"))

    def access(self, path):
        p = self._path(path)
        if not p.exists():
            # Again, win32 doesn't report a sane error after, so let's fail
            # early if we can
            return defer.fail(FileNotFoundError(path))
        # For now, just see if we can os.listdir() it
        try:
            p.listdir()
        except (IOError, OSError), e:
            return errnoToFailure(e.errno, path)
        except:
            return defer.fail()
        else:
            return defer.succeed(None)

    def stat(self, path, keys=()):
        p = self._path(path)
        if p.isdir():
            try:
                statResult = self._statNode(p, keys)
            except (IOError, OSError), e:
                return errnoToFailure(e.errno, path)
            except:
                return defer.fail()
            else:
                return defer.succeed(statResult)
        else:
            return self.list(path, keys).addCallback(lambda res: res[0][1])

    def list(self, path, keys=()):
        """
        Return the list of files at given C{path}, adding C{keys} stat
        informations if specified.

        @param path: the directory or file to check.
        @type path: C{str}

        @param keys: the list of desired metadata
        @type keys: C{list} of C{str}
        """
        filePath = self._path(path)
        if filePath.isdir():
            entries = filePath.listdir()
            fileEntries = [filePath.child(p) for p in entries]
        elif filePath.isfile():
            entries = [
                os.path.join(
                    *
                    filePath.segmentsFrom(
                        self.filesystemRoot))]
            fileEntries = [filePath]
        else:
            return defer.fail(FileNotFoundError(path))

        results = []
        for fileName, filePath in zip(entries, fileEntries):
            ent = []
            results.append((fileName, ent))
            if keys:
                try:
                    ent.extend(self._statNode(filePath, keys))
                except (IOError, OSError), e:
                    return errnoToFailure(e.errno, fileName)
                except:
                    return defer.fail()

        return defer.succeed(results)

    def _statNode(self, filePath, keys):
        """
        Shortcut method to get stat info on a node.

        @param filePath: the node to stat.
        @type filePath: C{filepath.FilePath}

        @param keys: the stat keys to get.
        @type keys: C{iterable}
        """
        filePath.restat()
        return [getattr(self, '_stat_' + k)(filePath) for k in keys]

    def _stat_size(self, fp):
        """
        Get the filepath's size as an int

        @param fp: L{twisted.python.filepath.FilePath}
        @return: C{int} representing the size
        """
        return fp.getsize()

    def _stat_permissions(self, fp):
        """
        Get the filepath's permissions object

        @param fp: L{twisted.python.filepath.FilePath}
        @return: L{twisted.python.filepath.Permissions} of C{fp}
        """
        return fp.getPermissions()

    def _stat_hardlinks(self, fp):
        """
        Get the number of hardlinks for the filepath - if the number of
        hardlinks is not yet implemented (say in Windows), just return 0 since
        stat-ing a file in Windows seems to return C{st_nlink=0}.

        (Reference:
        U{http://stackoverflow.com/questions/5275731/os-stat-on-windows})

        @param fp: L{twisted.python.filepath.FilePath}
        @return: C{int} representing the number of hardlinks
        """
        try:
            return fp.getNumberOfHardLinks()
        except NotImplementedError:
            return 0

    def _stat_modified(self, fp):
        """
        Get the filepath's last modified date

        @param fp: L{twisted.python.filepath.FilePath}
        @return: C{int} as seconds since the epoch
        """
        return fp.getModificationTime()

    def _stat_owner(self, fp):
        """
        Get the filepath's owner's username.  If this is not implemented
        (say in Windows) return the string "0" since stat-ing a file in
        Windows seems to return C{st_uid=0}.

        (Reference:
        U{http://stackoverflow.com/questions/5275731/os-stat-on-windows})

        @param fp: L{twisted.python.filepath.FilePath}
        @return: C{str} representing the owner's username
        """
        try:
            userID = fp.getUserID()
        except NotImplementedError:
            return "0"
        else:
            if pwd is not None:
                try:
                    return pwd.getpwuid(userID)[0]
                except KeyError:
                    pass
            return str(userID)

    def _stat_group(self, fp):
        """
        Get the filepath's owner's group.  If this is not implemented
        (say in Windows) return the string "0" since stat-ing a file in
        Windows seems to return C{st_gid=0}.

        (Reference:
        U{http://stackoverflow.com/questions/5275731/os-stat-on-windows})

        @param fp: L{twisted.python.filepath.FilePath}
        @return: C{str} representing the owner's group
        """
        try:
            groupID = fp.getGroupID()
        except NotImplementedError:
            return "0"
        else:
            if grp is not None:
                try:
                    return grp.getgrgid(groupID)[0]
                except KeyError:
                    pass
            return str(groupID)

    def _stat_directory(self, fp):
        """
        Get whether the filepath is a directory

        @param fp: L{twisted.python.filepath.FilePath}
        @return: C{bool}
        """
        return fp.isdir()


class WIOFTPShell(WIOFTPAnonymousShell):

    """
    An authenticated implementation of L{IFTPShell}.
    """

    # USERS ARE NOT ALLOWED TO CREATE DIRECTORY
    #

    # NEW REQUEST CORRESPOND TO DIRECTORY
    def makeDirectory(self, path):
        p = self._path(path)
        try:
            p.makedirs()
        except (IOError, OSError), e:
            return errnoToFailure(e.errno, path)
        except:
            return defer.fail()
        else:
            return defer.succeed(None)

    # def removeDirectory(self, path):
    #    p = self._path(path)
    #    if p.isfile():
    #        # Win32 returns the wrong errno when rmdir is called on a file
    #        # instead of a directory, so as we have the info here, let's fail
    #        # early with a pertinent error
    #        return defer.fail(IsNotADirectoryError(path))
    #    try:
    #        os.rmdir(p.path)
    #    except (IOError, OSError), e:
    #        return errnoToFailure(e.errno, path)
    #    except:
    #        return defer.fail()
    #    else:
    #        return defer.succeed(None)

    # def removeFile(self, path):
    #    p = self._path(path)
    #    if p.isdir():
    #       # Win32 returns the wrong errno when remove is called on a
    #        # directory instead of a file, so as we have the info here,
    #        # let's fail early with a pertinent error
    #        return defer.fail(IsADirectoryError(path))
    #    try:
    #        p.remove()
    #    except (IOError, OSError), e:
    #        return errnoToFailure(e.errno, path)
    #    except:
    #        return defer.fail()
    #    else:
    #        return defer.succeed(None)

    # def rename(self, fromPath, toPath):
    #    fp = self._path(fromPath)
    #    tp = self._path(toPath)
    #    try:
    #        os.rename(fp.path, tp.path)
    #    except (IOError, OSError), e:
    #        return errnoToFailure(e.errno, fromPath)
    #    except:
    #        return defer.fail()
    #    else:
    #        return defer.succeed(None)

    def openForWriting(self, path):
        """
        Open C{path} for writing.

        @param path: The path, as a list of segments, to open.
        @type path: C{list} of C{unicode}
        @return: A L{Deferred} is returned that will fire with an object
            implementing L{IWriteFile} if the file is successfully opened.  If
            C{path} is a directory, or if an exception is raised while trying
            to open the file, the L{Deferred} will fire with an error.
        """
        p = self._path(path)
        if p.isdir():
            # Normally, we would only check for EISDIR in open, but win32
            # returns EACCES in this case, so we check before
            return defer.fail(IsADirectoryError(path))
        try:
            fObj = p.open('w')
        except (IOError, OSError), e:
            return errnoToFailure(e.errno, path)
        except:
            return defer.fail()
        return defer.succeed(_FileWriter(fObj))

    def logout(self):
        pass


class WIOBaseFTPRealm:

    """
    Base class for simple FTP realms which provides an easy hook for specifying
    the home directory for each user.
    """
    implements(portal.IRealm)

    def __init__(self, anonymousRoot):
        self.anonymousRoot = filepath.FilePath(anonymousRoot)

    def getHomeDirectory(self, avatarId):
        """it on startup. FTPFactory will start up a twisted.protocols.ftp.FTP()
        Return a L{FilePath} representing the home directory of the givenhandler for each incoming OPEN request. Business as usual in Twisted land.
        avatar.  Override this in a subclass.
        @rtype: L{FilePath}and start the event loop.
       """
        raise NotImplementedError(
            "%r did not override getHomeDirectory" % (self.__class__,))

    def requestAvatar(self, avatarId, mind, *interfaces):
        # < OK THIS WHERE WE CREATE THE SHELLS
        for iface in interfaces:
            if iface is IFTPShell:
                if avatarId is checkers.ANONYMOUS:
                    avatar = FTPAnonymousShell(self.anonymousRoot)
                else:
                    avatar = FTPShell(self.getBucketId(avatarId))
                return (IFTPShell, avatar,
                        getattr(avatar, 'logout', lambda: None))
        raise NotImplementedError(
            "Only IFTPShell interface is supported by this realm")


class WIOFTPRealm(WIOBaseFTPRealm):

    """
    @type anonymousRoot: L{twisted.python.filepath.FilePath}
    @ivar anonymousRoot: Root of the filesystem to which anonymous
        users will be granted access.

    @type bucket_id: L{filepath.FilePath}
    @ivar bucket_id: Root of the filesystem containing user home directories.
    """

    def __init__(self, anonymousRoot, database):
        WIOBaseFTPRealm.__init__(self, anonymousRoot)
        self.database = database
        bucket_id_ = aiosciweb1.wioframework.bucket_alloc()
        self.bucket_id = bucket_id  # filepath.FilePath(bucket_id)

    def get_bucket_id(self, avatarId):
        """
        Use C{avatarId} as a single path segment to construct a child of
        C{self.bucket_id} and return that child.
        """
        return self.bucket_id  # .child(avatarId)


if __name__ == "__main__":
    #p = Portal(WIOFTPRealm("public_ftp",self.ftpconnection), [AllowAnonymousAccess(), WIOPasswordDB("pass.dat")])
    dbconnection = False
    p = Portal(
        WIOFTPRealm(
            "public_ftp", dbconnection), [
            AllowAnonymousAccess(), FilePasswordDB("pass.dat")])

    #
    # Once the portal is set up, start up the FTPFactory and pass the portal to
    # it on startup. FTPFactory will start up a twisted.protocols.ftp.FTP()
    # handler for each incoming OPEN request. Business as usual in Twisted land.
    #
    f = FTPFactory(p)

    #
    # You know this part. Point the reactor to port 21 coupled with the above factory,
    # and start the event loop.
    #
    reactor.listenTCP(21, f)
    reactor.run()
